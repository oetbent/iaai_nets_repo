

### Requirements ###

* http://jupyter.org/install
* step through example.ipynb
* play!

### Contact ###

* Please Contact us as first point for any help or questions in running, extending and contributing to this work!
* oetbent@robots.ox.ac.uk

### Blog ###

https://www.ibm.com/blogs/think/2018/02/ai-malaria/
https://www.businessdailyafrica.com/analysis/columnists/Benefits-of-using-AI-to-combat-malaria/4259356-4304724-14bbc6hz/
https://longitudes.ups.com/how-ai-is-helping-control-malaria/

### Paper ###

http://www.aaai.org/GuideBook2018/16148-73758-GB.pdf
