import numpy as np
from libs.environment import *
from libs.location import *
from libs.visualisation import *

from sys import exit, exc_info, argv
from multiprocessing import Pool, current_process

baseuri = "https://openmalaria-v1.mybluemix.net"
userId= ""
locations = showLocations(baseuri)
locationId = getLocationId(locations, 'workshop_scenario_action')

realworkercount=int(argv[1]) if len(argv) > 1 else  1; #determines how many requests can actually be concurrently made

def individual_get_score(gene):
    name_s=current_process().name
    if  "-" in name_s:
	    id = np.mod(int(name_s.split("-")[1]),realworkercount)
    else:
            id = -1
    try:
        envId = initEnv(locationId, userId, baseuri);
        reward = postAction(envId, gene%1, baseuri)
        #print(id, gene%1, reward)

    except:
        print(exc_info(),gene)
        reward = None;
    
    return reward

def evaluate(data):
    if len(data.shape) == 2: #vector of chromosomes
        pool = Pool(realworkercount)
        result = pool.map(individual_get_score, data)
        pool.close()
        pool.join()
    else:
        result = individual_get_score(data)
    return result

from es import OpenES #pip install git+https://github.com/slremy/estool

def modifier(individuals):
    processed = np.clip(np.abs(individuals),[0.1,0.1,0],[0.99,0.99,19]);
    processed[:,-1]=processed[:,-1].astype(int);
    return processed

def mutate(chromosome):
    mutation_rate = .2
    for j in xrange(num_paramters):
        r = np.random.rand(1);
        if(r > mutation_rate):
            chromosome[j] = (chromosome[j])+(np.random.randn(1));
    return chromosome

def make_random_individuals(x,y):
    value=np.hstack((np.random.rand(x,y-1),np.random.randint(20, size=x).reshape(-1,1)));
    return value

popsize=64
num_paramters=3
num_generations = 10

exp_location
filename = "OpenESdata-%d-%d-%s.npy"%(ts,popsize,exp_location)
# defines genetic algorithm solver
solver = OpenES(num_paramters,     # number of model parameters
            random_individuals_fcn=make_random_individuals,
              sigma_init=0.5,        # initial standard deviation
              popsize=popsize,       # population size
              forget_best=False,     # forget the historical best elites
              weight_decay=0.00,     # weight decay coefficient
              )

history=  np.empty(shape=(num_generations, popsize, num_paramters+1))
for i in range(num_generations):
    try:
        # ask the ES to give us a set of candidate solutions
        solutions = solver.ask(modifier);
        
        # calculate the reward for each given solution using our own method
        rewards = evaluate(solutions)
        
        # give rewards back to ES
        solver.tell(rewards)

        # get best parameter, reward from ES
        reward_vector = solver.result()
        print(reward_vector[1],i, num_generations)
        history[i,:,:] = np.hstack((solutions,np.array(rewards).reshape(-1,1)))
        np.save(filename, history)
        
    except (KeyboardInterrupt, SystemExit):
        print(exc_info())